import  youtube_dl, discord, json #derulo
from discord.ext import commands # for .commands()
import sys


# NOTE: admin only commands are not protected because i'm too lazy to google it
class textCommands:
	def __init__(self, bot):
		self.bot = bot
	
	# helper fucntion to cut down on clutter for admin sec commands
	def is_admin(self, ctx):
		return ctx.message.author.server_permissions.administrator

	# discord 's default help command overwritten in bot.py
	@commands.command(name='help')
	async def help(self):
		with open('info.json') as file:
			data = json.load(open('info.json'))
		
		await self.bot.say(data['general']['help_str'])
	
	@commands.command(name='bot')
	async def bot_(self):
		with open('info.json') as file:
			data = json.load(open('info.json'))
		
		await self.bot.say(data['general']['bot'])
		
	# here we handle the custom command capablility of the bot
	@commands.command(name='c')
	async def custom_reply(self, arg):
		try:
			with open('commands.json','r') as file:
				data = json.load(file)	

			await self.bot.say(data[arg])
		# if nothing was found print a useful bad input string
		# this part is not neccessary, we can always just return to avoid this
		except KeyError as k:
			print('{} raise by {} as input: Likely bad input'.format(type(k), arg))
	
	# permissions must equate to 0x08 according to discord's docs for admin server perms
	@commands.group(name='cedit',pass_context=True)
	async def supra_reply(self, ctx):
		if not self.is_admin(ctx):
			return 
		if ctx.invoked_subcommand is None:
			# assuming proper permission are reached we may show some info
			with open('info.json') as file:
				data = json.load(open('info.json'))
			result_string = ''
			for i in data['admin']:
				result_string += data['admin'][i] +'\n'	
			await self.bot.say('```\n'+result_string+'```\n')

	@supra_reply.command(name='mk', pass_context=True)
	async def reply_make(self, ctx, input_arg, out_arg):
		if not self.is_admin(ctx):
			print('non-admin attempt to create {0.name} : {0.name}'.foratm(input_arg, out_arg))
			return 
		# open and read file into some temp dict
		with open('commands.json','r') as file:
			data = json.load(file)
		# next we update our temporary dictionary with the new data
		data.update({input_arg:out_arg})
		# the we update the database 
		with open('commands.json', 'w') as outafile:
			json.dump(data, outfile, indent=4)
		await self.bot.say('Successfully updated commands!')

	# admin command - removes a given command via key input
	@supra_reply.command(name='rm', pass_context=True)
	async def reply_rm(self, ctx, c_arg):
		if not self.is_admin(ctx):
			print('Non-admin attempt to remove command {0.name}'.format(c_arg))
			return 
		# load the actual db file into memory
		with open('commands.json', 'r') as infile:
			data = json.load(infile)
		# next we update the actual temp dictionary by cleaning out the key/value pair
		if c_arg in data: del data[c_arg]
		# update the libary
		with open('commands.json', 'w') as outfile:
			json.dump(data, outfile, indent=4)
		print('successfully removed {0.name} pair'.format(c_arg))
	# SYNC VIDEO COMMAND - allows users to create 'rooms' to watch videos together
	# sync-video.com supports youtube/vimeo at the moment
	@commands.command
	async def sync(self, vid_link : str):
		# nested helper method to determine whether a parsed input is valid 
		def valid_link(link):
			link_c = youtube_dl.extractor.gen_extractor_classes()
			for i in link_c:
				if i.suitable(link) and i.IE_NAME != 'generic':
					return True
			return False
		# sync-video only accepts certain types of links see: 'normal'
		if valid_link(vid_link):
			# check for shortened links which don't actually work btw
			if 'youtu.be' in vid_link:
				await self.bot.say('Shortened links are not valid!')
			# at this point we should give outa good video room
			new_link = 'http.//sync-' + vid_link.split('www.')[1]
			await self.bot.say('`Your room is ready for viewing: `' + new_link)
		else:
			await self.bot.say('Invalid Link! Youtube Links only')
	
def setup(bot):
	bot.add_cog(textCommands(bot))
