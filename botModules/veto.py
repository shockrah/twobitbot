# map veto for various games
import time, json #derulo

#TODO: timer setup withint the veto module and the client timer

class veto:
    def __init__(self, bot):
        self.bot = bot
        self.veto_started = False
        self.map_list = None
        # actual timer var used for individual turns
        self.veto_timer = None 
        self.turn_timer = 60 # seconds max time per turn

    # we take an argument 
    @commands.command(name='veto')
    async def start_veto(self, game_arg):
        if veto_started is False:
            # set the veto flag to show a veto 
            self.veto_started = True
            with open('maps.json', 'r') as file:
                m_list = json.load(file)
            
            await self.bot.say('60s per turn!\n'+m_list[game_arg])
            self.veto_timer = time.time()
            except KeyError as k:
                print('caught {} - likely bad user input'.format(type(k)))
                await self.bot.say('Available games: **csgo\t\"quake duel\"**')

    @commands.command(name='ban')
    async def veto_ban(self, map_arg):
        try:
            self.map_list.remove(map_arg)
            await self.bot.say('Remaining maps: '+self.map_list)
        except ValueError as e:
            print('caught {} - bad user input'.format(type(e)))
            await self.bot.say('Map not found! :grimacing: ')
